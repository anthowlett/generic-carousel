# Publicis.Sapient Front End Senior Associate Test

## Introduction
Thank you for taking the time to review my technical test.

I used [Create React App](https://github.com/facebook/create-react-app) as a starting point and looked to create an enjoyable user experience while maintaining code simplicity and efficiency. The state is managed using [Hooks](https://reactjs.org/docs/hooks-state.html?no-cache=1). I also used [styled-components](styled-components.com) to provide a SASS like syntax for styling UI elements. This repository has been configured with a [GitFlow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) structure for easy collaboration.

I've also utilised [Jest](https://jestjs.io/) with [Enzyme](https://airbnb.io/enzyme/) for unit tests and [Storybook](https://storybook.js.org/) for visual UI testing. The coverage isn't as extensive as I would have liked, but hopefully is enough to demonstrate the intention.

To ensure coding standards across developers and to reduce the chance of badly formed code creeping into the project I've also implemented a precommit hook using [Husky](https://github.com/typicode/husky) with code formatting by [Prettier](https://prettier.io/).

## Prerequisites

* [NodeJS](https://nodejs.org/en/)
* [NPM](https://docs.npmjs.com/cli/install)

## NPM packages

* axios (^0.18.0)
* prop-types (^15.7.2)
* react (^16.7.0)
* react-dom (^16.7.0)
* react-scripts (2.1.3)
* styled-components (^4.1.3)
* styled-reset (^4.0.9)

* enzyme (^3.11.0)
* enzyme-adapter-react-16 (^1.15.2)
* @storybook/react (^5.3.6)
* husky (^4.0.10)
* prettier (^1.19.1)

## Getting started

1. Clone this repository to your local machine

2. Install dependencies

	- `npm i`

2. Start up development environment

	- `npm start`

## Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run storybook`

Launches Storybook in the interactive watch mode.<br>
Open [http://localhost:9009](http://localhost:9009) to view it in the browser.<br>
This allows you to test any code changes against the various states of the component to ensure the UI is responding correctly.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.