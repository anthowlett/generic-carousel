import { createGlobalStyle } from 'styled-components';
import reset from 'styled-reset';
import RopaSansTTF from './fonts/Ropa_Sans/RopaSans-Regular.ttf';

const GlobalStyle = createGlobalStyle`
  ${reset}

  @font-face {
    font-family: 'Ropa Sans';
    src: url(${RopaSansTTF}) format('truetype');
  }

  *:focus {
    outline: none;
  }

  html {
    box-sizing: border-box;
  }

  *, *:before, *:after {
    box-sizing: inherit;
  }

  body {
    font-family: 'Ropa Sans', sans-serif;
    font-size: 16px;
    background-color: #eee;
    -webkit-font-smoothing: antialiased;
  }
`;

export default GlobalStyle;
