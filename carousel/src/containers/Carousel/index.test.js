import React from 'react';
import { shallow } from 'enzyme';
import Carousel from '.';
import Loading from '../../components/Loading';
import * as Styled from './styles';
import '../../setupTests';

export const MOCK_ITEMS = [
  { id: 1, imageURL: 'https://picsum.photos/id/1021/600', user: 'Test User 1', likes: 5 },
  { id: 2, imageURL: 'https://picsum.photos/id/1015/600', user: 'Test User 2', likes: 15 },
  { id: 3, imageURL: 'https://picsum.photos/id/1016/600', user: 'Test User 3', likes: 10 },
  { id: 4, imageURL: 'https://picsum.photos/id/1018/600', user: 'Test User 4', likes: 20 },
  { id: 5, imageURL: 'https://picsum.photos/id/1019/600', user: 'Test User 5', likes: 5 },
  { id: 6, imageURL: 'https://picsum.photos/id/1020/600', user: 'Test User 6', likes: 10 },
];

describe('Carousel Container', () => {
  it('should render', () => {
    const component = shallow(<Carousel items={MOCK_ITEMS} />);

    expect(component.html()).toBeTruthy();
    expect(component.debug()).toMatchSnapshot();
  });

  it('should display loading state', () => {
    const component = shallow(<Carousel items={[]} />);

    expect(component.find(Loading)).toHaveLength(1);
  });

  it('should display an error', () => {
    const component = shallow(<Carousel items={null} />);

    expect(component.find(Styled.Error)).toHaveLength(1);
  });
});