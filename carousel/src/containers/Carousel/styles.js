import styled from 'styled-components';
import { COLOURS } from '../../utils';

export const Container = styled.div`
  background-color: ${COLOURS.WHITE};
`;

export const Error = styled.p`
  margin: 15px 10px;
  padding: 10px;
  color: ${COLOURS.RED};
`;