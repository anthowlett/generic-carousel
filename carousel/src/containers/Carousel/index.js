import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Carousel from '../../components/Carousel';
import Loading from '../../components/Loading';
import * as Styled from './styles';

/**
 * CarouselContainer
 * 
 * Handles data fetching and transforms then passes
 * an array of items to the presentational layer.
 */

const CarouselContainer = ({ items }) => {
  const [active, setActive] = useState(0);

  if (!items) {
    return <Styled.Error>An error occurred. Please check your internet connection.</Styled.Error>
  } 

  if (!items.length) {
    return <Loading />
  } 
  
  return (
    <Styled.Container>
      <Carousel items={items} active={active} changeSlide={setActive} />
    </Styled.Container>
  );
};

CarouselContainer.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    imageURL: PropTypes.string.isRequired,
    user: PropTypes.string,
    likes: PropTypes.number
  })),
};

CarouselContainer.defaultProps = {
  items: []
};

export default CarouselContainer;
