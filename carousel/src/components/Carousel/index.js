import React from 'react';
import PropTypes from 'prop-types';
import Slide from '../Slide';
import * as Styled from './styles';

/**
 * Carousel
 * 
 * Responsible for the presentation of the carousel and
 * rendering of the slides.
 */

const Carousel = ({ items, active, changeSlide }) => {
  const disabledNext = (currentIndex) => {
    const viewportWidth = window.innerWidth;
  
    // If small screens, offset by 3 visible
    if (viewportWidth > 600 && viewportWidth <= 1000) {
      return currentIndex === items.length - 3;
    }

    // If large screens, offset by 5 visible
    if (viewportWidth > 1000) {
      return currentIndex === items.length - 5;
    }

    // If mobile, check if last item
    return currentIndex === items.length-1;
  };

  // If viewport resized, reset carousel to first slide
  // n.b. this could be extended to change the index to the
  // max possible rather than just default to the first
  window.onresize = () => changeSlide(0);

  return (
    <Styled.Container>
      <Styled.Inner active={active} total={items.length}>
        {items.map(item => <Slide key={item.id} item={item} />)}
      </Styled.Inner>
      <Styled.Actions>
        <Styled.Button type="prev" onClick={() => changeSlide(active-1)} disabled={active === 0}>Prev</Styled.Button>
        <Styled.Button type="next" onClick={() => changeSlide(active+1)} disabled={disabledNext(active)}>Next</Styled.Button>
      </Styled.Actions>
    </Styled.Container>
  );
};

Carousel.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    imageURL: PropTypes.string.isRequired,
    user: PropTypes.string,
    likes: PropTypes.number
  })).isRequired,
  active: PropTypes.number,
  changeSlide: PropTypes.func
};

Carousel.defaultProps = {
  active: 0,
  changeSlide: index => index,
}

export default Carousel;
