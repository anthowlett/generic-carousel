import styled, { css } from 'styled-components';
import { COLOURS } from '../../utils';

export const Container = styled.div`
  overflow: hidden;
  position: relative;
`;

export const Inner = styled.div`
  display: flex;
  width: ${({ total = 1 }) => `${total*100}%`};
  transform: ${({ active = 0, total = 1 }) => `translateX(${active*-100/total}%)`};
  transition: transform 0.25s ease-in-out;

  @media (min-width: 600px) {
    width: ${({ total = 1 }) => `${total/3*100}%`};
  }

  @media (min-width: 1000px) {
    width: ${({ total = 1 }) => `${total/5*100}%`};
  }
`;

export const Actions = styled.div`
  @media (min-width: 600px) {
    display: flex;
    justify-content: center;
    padding: 15px;
  }
`;

export const Button = styled.button`
  display: flex;
  justify-content: ${({ type }) => type === 'prev' ? 'flex-end' : 'flex-start'};
  align-items: center;
  position: absolute;
  top: 50%;
  ${({ type }) => type === 'next' && css`right: 15px`};
  ${({ type }) => type === 'prev' && css`left: 15px`};
  z-index: 2;
  width: 150px;
  height: 150px;
  padding: 10px;
  font: inherit;
  appearance: none;
  border: 0;
  border-radius: 50%;
  color: ${COLOURS.WHITE};
  cursor: pointer;
  transform: translate(${({ type }) => type === 'prev' ? '-70%' : '70%'}, -50%) ${({ type }) => type === 'prev' ? 'rotate(180deg)' : ''};
  transition: 0.25s ease-in-out;
  background: rgba(0, 0, 0, 0.5) url(images/arrow.svg) no-repeat center left 25px / 20%;
  text-indent: -999em;

  @media (min-width: 600px) {
    position: initial;
    width: auto;
    height: auto;
    margin: 2px;
    padding: 15px 20px;
    background: linear-gradient(270deg, ${COLOURS.RED}, ${COLOURS.BLACK});
    background-size: 200%;
    border-radius: ${({ type }) => type === 'prev' ? '4px 0 0 4px' : '0 4px 4px 0'};
    transform: none;
    text-indent: 0;
  }

  &:disabled {
    opacity: 0.25;
    cursor: not-allowed;
  }

  &:hover {
    @media (min-width: 600px) {
      background-position: 50%;
    }
  }
`;