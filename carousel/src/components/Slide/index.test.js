import React from 'react';
import { shallow } from 'enzyme';
import Slide from '.';
import * as Styled from './styles';
import '../../setupTests';

export const MOCK_ITEM = { 
  id: 1, 
  imageURL: 'https://picsum.photos/id/1021/600', 
  user: 'Test User 1', 
  likes: 5 
};

describe('Slide', () => {
  it('should render', () => {
    const component = shallow(<Slide item={MOCK_ITEM} />);

    expect(component.html()).toBeTruthy();
    expect(component.debug()).toMatchSnapshot();
  });

  it('should not render without an image', () => {
    const component = shallow(<Slide item={{ ...MOCK_ITEM, imageURL: undefined }} />);

    expect(component.html()).toBeNull();
  });

  it('should render without likes', () => {
    const component = shallow(<Slide item={{ ...MOCK_ITEM, likes: undefined }} />);

    expect(component.find(Styled.Info)).toHaveLength(1);
  });
});