import React from 'react';
import PropTypes from 'prop-types';
import * as Styled from './styles';

/**
 * Slide
 * 
 * Renders the contents of each slide
 */

const Slide = ({ item }) => {
  const { imageURL, user = 'Anon', likes = 0 } = item;

  if (!imageURL) {
    return null;
  }

  return (
    <Styled.Container>
      <Styled.Image src={imageURL} alt={user} />
      <Styled.Info>
        <h3>{user}</h3>
        <p>&hearts; {likes}</p>
      </Styled.Info>
    </Styled.Container>
  );
};

Slide.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number,
    imageURL: PropTypes.string,
    user: PropTypes.string,
    likes: PropTypes.number
  }).isRequired
};

export default Slide;
