import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 15px;
`;

export const Image = styled.img`
  display: block;
  max-width: 100%;
  margin: auto;
  border-radius: 4px;
`;

export const Info = styled.div`
  display: flex;
  justify-content: space-between;
  padding-top: 10px;
  text-align: center;
`;