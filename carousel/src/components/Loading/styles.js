/* eslint-disable import/prefer-default-export */
import styled, { keyframes } from 'styled-components';

const rippleAnimation = ({ size }) => keyframes`
    0% {
        top: ${size / 2.25}px;
        left: ${size / 2.25}px;
        width: 0;
        height: 0;
        opacity: 1;
    }
    100% {
        top: ${size / 20}px;
        left: ${size / 20}px;
        width: ${size * 0.75}px;
        height: ${size * 0.75}px;
        opacity: 0;
    }
`;

export const Container = styled.div`
  width: ${({ size }) => size}px;
  height: ${({ size }) => size}px;
  margin: auto;
  transform: scale(0.7);
  position: relative;

  div {
    box-sizing: content-box;
    position: absolute;
    border-width: ${({ size }) => `${size / 20}px` || '10px'};
    border-style: solid;
    border-color: ${({ color }) => color};
    opacity: 1;
    border-radius: 50%;
    animation: ${(props) => rippleAnimation(props)} 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;

    &::nth-child(2) {
      border-color: #ffffff;
      animation-delay: -0.5s;
    }
  }
`;
