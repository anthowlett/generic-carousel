import React from 'react';
import PropTypes from 'prop-types';
import * as Styled from './styles';

const Loading = (props) => (
  <Styled.Container {...props}>
    <div />
    <div />
  </Styled.Container>
);

Loading.propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
};

Loading.defaultProps = {
  color: '#fe414d',
  size: 150,
};

export default Loading;
