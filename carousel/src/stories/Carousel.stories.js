import React from 'react';
import { withKnobs, object } from '@storybook/addon-knobs';
import GlobalStyle from '../GlobalStyles';
import Carousel from '../containers/Carousel';

const globalStyles = s => <><GlobalStyle />{s()}</>;

export default {
  title: 'Carousel',
  component: Carousel,
  decorators: [withKnobs, globalStyles],
};

const MOCK_ITEMS = [
  { id: 1, imageURL: 'https://picsum.photos/id/1021/600', user: 'Test User 1', likes: 5 },
  { id: 2, imageURL: 'https://picsum.photos/id/1015/600', user: 'Test User 2', likes: 15 },
  { id: 3, imageURL: 'https://picsum.photos/id/1016/600', user: 'Test User 3', likes: 10 },
  { id: 4, imageURL: 'https://picsum.photos/id/1018/600', user: 'Test User 4', likes: 20 },
  { id: 5, imageURL: 'https://picsum.photos/id/1019/600', user: 'Test User 5', likes: 5 },
  { id: 6, imageURL: 'https://picsum.photos/id/1020/600', user: 'Test User 6', likes: 10 },
];

export const Standard = () => <Carousel items={object('Items', MOCK_ITEMS)} />
export const Loading = () => <Carousel items={[]} />
export const Error = () => <Carousel items={null} />