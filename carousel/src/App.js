import React, { useState } from 'react';
import axios from 'axios';
import styled from 'styled-components';
import GlobalStyle from './GlobalStyles';
import Carousel from './containers/Carousel';
import { API_KEY, QUERY } from './utils';

const Title = styled.h1`
  margin: 1.5rem 1rem;
  font-size: 2rem;
`;

const App = () => {
  const [items, setItems] = useState([]);
  const getItems = async () => {
    const { data } = await axios.get(`https://pixabay.com/api/?key=${API_KEY}&q=${QUERY}&image_type=photo&per_page=10`);
    const { hits } = data;
    const transformItem = item => ({ ...item, imageURL: item.webformatURL });
  
    if (hits && hits.length) {
      setItems(hits.map(transformItem));
      return hits;
    }
    
    setItems(null);
    return null;
  };

  if (items && !items.length) {
    getItems();
  }

  return (
    <React.Fragment>
      <GlobalStyle />
      <Title>Carousel Test</Title>
      <Carousel items={items} />
    </React.Fragment>
  );
};

export default App;
