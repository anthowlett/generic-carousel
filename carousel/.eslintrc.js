module.exports = {
  extends: ['airbnb', 'prettier', 'prettier/react'],
  rules: {
    'react/jsx-filename-extension': ['error', { extensions: ['.js'] }], // allow .js files, instead of .jsx
    'react/jsx-props-no-spreading': [0, { 'html': 'ignore' }],
    'react/jsx-fragments': 0,
    'no-irregular-whitespace': 0, // conflict with prettier
  },
  plugins: ['react', 'jsx-a11y', 'import'],
  env: {
    browser: true,
  },
  parser: 'babel-eslint',
};
